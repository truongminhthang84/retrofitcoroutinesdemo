package com.sapo.retrofitdemo.data

import retrofit2.http.GET
import retrofit2.http.Path

interface GitHubApi {
    @GET("users/{user}/repos")
    suspend fun getRepos(@Path("user") user: String): List<Repo>
}