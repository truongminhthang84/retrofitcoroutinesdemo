package com.sapo.retrofitdemo.data

import android.accounts.NetworkErrorException
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.sapo.retrofitdemo.Injector
import kotlinx.coroutines.Dispatchers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RemoteRepositoryImpl : RemoteRepository {
    private val api = Retrofit.Builder()
        .baseUrl("https://api.github.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(GitHubApi::class.java)

    override fun getRepos(user: String): LiveData<Result<List<Repo>>> {
        return liveData {
               try {
                   val repos = api.getRepos("TruongMinhThang")
                   val result = Result.success(repos)
                   emit(result)
            } catch (e: Exception) {
                emit(Result.failure(NetworkErrorException(e.message)))
            }

        }


    }
}


