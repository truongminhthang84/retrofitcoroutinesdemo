package com.sapo.retrofitdemo.data

import androidx.lifecycle.LiveData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface RemoteRepository {
    fun getRepos(user: String): LiveData<Result<List<Repo>>>
}