package com.sapo.retrofitdemo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import com.sapo.retrofitdemo.adapter.RepoAdapter
import com.sapo.retrofitdemo.databinding.FragmentHomeBinding


open class BaseFragment: Fragment()

class HomeFragment : BaseFragment() {

    private lateinit var binding: FragmentHomeBinding

    private val viewModel: HomeViewModel by viewModels {
        Injector.provideHomeViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val adapter = RepoAdapter()
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        context ?: return binding.root
        binding.recyclerView.adapter = adapter
        setupUI(adapter, binding)
        return binding.root
    }

    private fun setupUI(adapter: RepoAdapter, binding: FragmentHomeBinding)  {
        viewModel.allRepos.observe(viewLifecycleOwner){

            if (it.isSuccess) {
                val list = it.getOrDefault(emptyList())
                binding.hasItem = list.isNotEmpty()
                adapter.submitList(list)
            } else {
                binding.hasItem = false
            }
        }
    }

}

