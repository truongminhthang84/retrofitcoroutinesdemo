package com.sapo.retrofitdemo

import android.accounts.NetworkErrorException
import androidx.lifecycle.*
import com.sapo.retrofitdemo.data.RemoteRepository
import com.sapo.retrofitdemo.data.RemoteRepositoryImpl
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class HomeViewModel(private val repository: RemoteRepository) : ViewModel() {
    // TODO: Implement the ViewModel
    var allRepos = repository.getRepos("TruongMinhThang")


    private val _spinner: MutableLiveData<Boolean> = MutableLiveData()

    val spinner: LiveData<Boolean>
       get() = _spinner
    private val _snackBar = MutableLiveData<String>()

    /**
     * Request a snackbar to display a string.
     */
    val snackbar: LiveData<String>
        get() = _snackBar

    fun updateData() {
        launchDataLoad{
           repository.getRepos("")
        }
    }

    private fun launchDataLoad(block: suspend () -> Unit): Job {
        return viewModelScope.launch {
            try {
                _spinner.value = true
                block()
            } catch (error: NetworkErrorException) {
                _snackBar.value = error.message
            } finally {
                _spinner.value = false
            }
        }
    }
}

class  HomeViewModelFactory  internal constructor (
    private val repository: RemoteRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return HomeViewModel(repository) as T
    }
}


object Injector
private fun Injector.getRemoteRepository(): RemoteRepository = RemoteRepositoryImpl
fun Injector.provideHomeViewModel(
): HomeViewModelFactory {
    val repository = getRemoteRepository()
    return HomeViewModelFactory(repository)
}